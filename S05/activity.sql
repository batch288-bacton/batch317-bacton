SELECT customerName FROM customers WHERE country = 'Philippines';

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';

SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

SELECT customerName FROM customers WHERE state IS NULL OR state = '';

SELECT firstName, lastName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';

SELECT customerName, country, creditLimit FROM customers WHERE country <> 'USA' AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country IN ('USA', 'France', 'Canada');

SELECT e.firstName, e.lastName, o.city FROM employees e JOIN offices o ON e.officeCode = o.officeCode WHERE o.city = 'Tokyo';

SELECT DISTINCT c.customerName FROM customers c JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber WHERE e.firstName = 'Leslie' AND e.lastName = 'Thompson';

SELECT p.productName, c.customerName FROM customers c JOIN orders o ON c.customerNumber = o.customerNumber JOIN orderdetails od ON o.orderNumber = od.orderNumber JOIN products p ON od.productCode = p.productCode WHERE c.customerName = 'Baane Mini Imports';

SELECT e.firstName AS EmployeeFirstName, e.lastName AS EmployeeLastName, c.customerName AS CustomerName, o.country AS OfficeCountry FROM employees e JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber JOIN offices o ON e.officeCode = o.officeCode WHERE c.country = o.country;

SELECT productName, quantityInStock FROM products JOIN productlines ON products.productLine = productlines.productLine WHERE productlines.productLine = 'Planes' AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE '%+81%';